package net.pl3x.bukkit.lib;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

public class Logger {
    private Plugin plugin;

    public Logger(Plugin plugin) {
        this.plugin = plugin;
    }

    private void log(String message) {
        message = ChatColor.translateAlternateColorCodes('&', "&3[&d" + plugin.getName() + "&3]&r " + message);
        if (!plugin.getConfig().getBoolean("color-logs", true)) {
            message = ChatColor.stripColor(message);
        }
        Bukkit.getServer().getConsoleSender().sendMessage(message);
    }

    public void debug(String message) {
        if (plugin.getConfig().getBoolean("debug-mode", false)) {
            log("&7[&eDEBUG&7]&e " + message);
        }
    }

    public void warn(String message) {
        log("&e[&6WARN&e]&6 " + message);
    }

    public void error(String message) {
        log("&e[&4ERROR&e]&4 " + message);
    }

    public void info(String message) {
        log("&e[&fINFO&e]&r " + message);
    }
}
